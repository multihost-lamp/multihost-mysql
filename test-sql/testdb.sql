CREATE TABLE testtable (
  name VARCHAR(20)
);

INSERT INTO testtable
  ( name)
VALUES
  ( 'one' ),
  ( 'two' ),
  ( 'three' );

