# Use the latest 5.7.x image
FROM mysql:5.7

# Where to put init SQL, in files named dbname.sql
VOLUME /init_sql

# Populate default values to fail if they are not set
ENV MHMYSQL_DBS fail_using_dockerfile_default

# Populate a safe-ish default that puts the root password in the logs
# TODO(asacamano@gmail.com) generate a random password and don't log it
ENV MYSQL_RANDOM_ROOT_PASSWORD yes

COPY src/mhmysql_init.sh /multihost_mysql/mhmysql_init.sh

CMD [ "/multihost_mysql/mhmysql_init.sh" ]

