#!/bin/bash

###################################################################################################
#
# Run in bash strict mode http://redsymbol.net/articles/unofficial-bash-strict-mode/
#

set -euo pipefail
IFS=$'\n\t'


###################################################################################################
#
# Pass commands directly through
#

if [[ ${1:-""} != "" ]]; then
  exec docker-entrypoint.sh "$@"
fi


###################################################################################################
#
# Set up variables
#

declare -A DB_USERS
declare -A USER_PASSWORDS

###################################################################################################
#
# Parse and validate environment variables
#

MHMYSQL_DBS=${MHMYSQL_DBS:-""}

if [[ $MHMYSQL_DBS == "" ]]; then
  echo "MHMYSQL_DBS must be defined"
  exit 1
fi

IFS=$' '
for db_def in ${MHMYSQL_DBS}; do
  IFS=$'\n\t'
  if [[ ${db_def} != *"="* ]]; then
    echo "Invalid database dev \"${db_def}\" in MHMYSQL_DBS"
    exit 1
  fi
  # see http://stackoverflow.com/questions/10638538
  dbname=${db_def%=*}
  user_def=${db_def#*=}
  if [[ ${dbname} == "" ]]; then
    echo "MHMYSQL_DBS has invalid entry missing database name : "$db_def
    exit 1
  elif [[ ${user_def} == "" ]]; then
    echo "MHMYSQL_DBS has invalid entry missing username and password : "$db_def
    exit 1
  fi

  username=${user_def%:*}
  password=${user_def#*:}
  if [[ ${username} == "" ]]; then
    echo "MHMYSQL_DBS has invalid entry missing username : "$db_def
    exit 1
  elif [[ ${password} == "" ]]; then
    echo "MHMYSQL_DBS has invalid entry missing password : "$db_def
    exit 1
  fi

  DB_USERS[${dbname}]=${username}
  USER_PASSWORDS[${dbname}:${username}]=${password}
done

###################################################################################################
#
# Log setup info to output
#

echo Done parsing env vars.
echo Setup is:

for db_name in ${!DB_USERS[@]}; do
  echo "  db \"${db_name}\" with users " ${DB_USERS[${db_name}]}
done

###################################################################################################
#
# Setup databases and users
#

MYSQL_DIR=/var/lib/mysql/
mkdir -p ${MYSQL_DIR}

for db_name in ${!DB_USERS[@]}; do
  db_init_file=${MYSQL_DIR}/.multihost-init.${db_name}
  db_setup_users_script=/docker-entrypoint-initdb.d/setup-${db_name}-01-users.sql
  db_setup_data_script=/docker-entrypoint-initdb.d/setup-${db_name}-02-data.sql
  if [[ -f ${db_init_file} ]]; then
    echo "${db_name} already set up - not running any setup scripts"
    echo "  rm ${db_setup_users_script}"
    echo "  rm ${db_setup_data_script}"
    rm -f ${db_setup_users_script} ${db_setup_data_script}
  else
    echo "${db_name} being setup"
    echo "  writing ${db_setup_users_script}"
    # Make the script that makes the database and the users
    echo "CREATE DATABASE ${db_name} CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;" > ${db_setup_users_script}
    username=${DB_USERS[${db_name}]}
    password=${USER_PASSWORDS[${db_name}:${username}]}
    echo "CREATE USER '${username}'@'%' IDENTIFIED BY '${password}';" >> ${db_setup_users_script}
    echo "GRANT ALL PRIVILEGES ON ${db_name}.* TO '${username}'@'%';" >> ${db_setup_users_script}
    echo "FLUSH PRIVILEGES;" >> ${db_setup_users_script}

    # Now copy the init script in place if its there
    db_setup_date_script_src=/init_sql/${db_name}.sql
    if [[ -f ${db_setup_date_script_src} ]]; then
      echo "  cat <useDB> ${db_setup_date_script_src} > ${db_setup_data_script}"
      echo "USE ${db_name};" > ${db_setup_data_script}
      cat ${db_setup_date_script_src} >> ${db_setup_data_script}
    fi

    echo "Created setup files ${db_setup_users_script} ${db_setup_data_script}" > ${db_init_file}
  fi
done


###################################################################################################
#
# Launch MySQL
#

exec docker-entrypoint.sh mysqld

