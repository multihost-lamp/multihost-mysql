# Multihost MySQL

Facilitates hosting multiple LAMP sites on a single mysql instance.

## Database configuration

The environment variablse MHMYSQL\_\* are used to configure this image.

CUrrently this happens once when /var/lib/mysql is initialized for the first time. Subsquent changes
to this variable will not be reflected in the database. (TODO(asacamano@gmail.com) Fix that.)

## MHMYSQL\_DBS

This contains a space-separated list of entries of the format

```
database_name=user:password
```

e.g.

```
wwwDb=wwwUser:SUPER_SECRET_PASSWORD bugDb=bugUser:ANOTHER_SUPER_SECRET_PASSWORD
```

## Data volume

This image, like the base mysql image, stores data in /var/lib/mysql, and callers should provide
their own volume to store that data persistently.

### Initialiation data

Each database is initialized once using the .sql file named db_name.sql mounted in /init_sql.

## Useful commands


### Testing changes to `mhmysql_init.sh`:

```sh
docker run -it --rm -v ${PWD}/test-sql:/init_sql -e MHMYSQL_DBS=testdb=testuser:testpassword docker.kasung.org/multihost-mysql /bin/bash
```

#### Running sql commands in a running container

```sh
docker-compose exec mhmysql /bin/bash
```

Note you can move data in and out through the /init_sql command if that directory is mounted in your
compose file, but it's better to use a reporting tool (phpmyadmin, etc) to get access to the data.

Once inside, you can run, for example

```sh
mysql <database_name> -u <username> -p
```

### Wiping your dev data

```sh
sudo rm -R runtime
```

## Security issues

Although the root password is random, so no process can log in as root, all initializeation scripts
run as root.  That means you can create users, grant them privileges, etc. This can be useful for
administration.
